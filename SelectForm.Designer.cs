﻿
namespace Tour
{
    partial class SelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectForm));
            this.label1 = new System.Windows.Forms.Label();
            this.panel_slide = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.panel_Help = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.panel_staff = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.panelManage = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.btnEmployyeDatabase = new System.Windows.Forms.Button();
            this.btnVehicalDatabase = new System.Windows.Forms.Button();
            this.btnHotelDatabase = new System.Windows.Forms.Button();
            this.btnLocationDatabase = new System.Windows.Forms.Button();
            this.btndataCus = new System.Windows.Forms.Button();
            this.btnRoute = new System.Windows.Forms.Button();
            this.btnTicket = new System.Windows.Forms.Button();
            this.btnTour = new System.Windows.Forms.Button();
            this.btnManage = new System.Windows.Forms.Button();
            this.panel_logo = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.picBackground = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.panel_slide.SuspendLayout();
            this.panel_Help.SuspendLayout();
            this.panel_staff.SuspendLayout();
            this.panelManage.SuspendLayout();
            this.panel_logo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBackground)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Yellow;
            this.label1.Location = new System.Drawing.Point(12, 141);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(243, 59);
            this.label1.TabIndex = 11;
            this.label1.Text = "label1";
            // 
            // panel_slide
            // 
            this.panel_slide.AutoScroll = true;
            this.panel_slide.BackColor = System.Drawing.Color.PaleTurquoise;
            this.panel_slide.Controls.Add(this.button7);
            this.panel_slide.Controls.Add(this.panel_Help);
            this.panel_slide.Controls.Add(this.button6);
            this.panel_slide.Controls.Add(this.panel_staff);
            this.panel_slide.Controls.Add(this.button4);
            this.panel_slide.Controls.Add(this.panelManage);
            this.panel_slide.Controls.Add(this.btnManage);
            this.panel_slide.Controls.Add(this.panel_logo);
            this.panel_slide.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_slide.Location = new System.Drawing.Point(0, 0);
            this.panel_slide.Name = "panel_slide";
            this.panel_slide.Size = new System.Drawing.Size(264, 640);
            this.panel_slide.TabIndex = 18;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.PaleTurquoise;
            this.button7.Dock = System.Windows.Forms.DockStyle.Top;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.Red;
            this.button7.Image = global::Tour.Properties.Resources.doorexit;
            this.button7.Location = new System.Drawing.Point(0, 1143);
            this.button7.Name = "button7";
            this.button7.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.button7.Size = new System.Drawing.Size(247, 54);
            this.button7.TabIndex = 8;
            this.button7.Text = "Exit";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // panel_Help
            // 
            this.panel_Help.Controls.Add(this.button1);
            this.panel_Help.Controls.Add(this.button5);
            this.panel_Help.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_Help.Location = new System.Drawing.Point(0, 1025);
            this.panel_Help.Name = "panel_Help";
            this.panel_Help.Size = new System.Drawing.Size(247, 118);
            this.panel_Help.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Linen;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(0, 60);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.button1.Size = new System.Drawing.Size(247, 58);
            this.button1.TabIndex = 2;
            this.button1.Text = "Guide";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Linen;
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Location = new System.Drawing.Point(0, 0);
            this.button5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button5.Name = "button5";
            this.button5.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.button5.Size = new System.Drawing.Size(247, 60);
            this.button5.TabIndex = 1;
            this.button5.Text = "About us";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Dock = System.Windows.Forms.DockStyle.Top;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.Black;
            this.button6.Image = global::Tour.Properties.Resources.help;
            this.button6.Location = new System.Drawing.Point(0, 976);
            this.button6.Name = "button6";
            this.button6.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.button6.Size = new System.Drawing.Size(247, 49);
            this.button6.TabIndex = 6;
            this.button6.Text = "Help";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // panel_staff
            // 
            this.panel_staff.Controls.Add(this.button2);
            this.panel_staff.Controls.Add(this.button3);
            this.panel_staff.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_staff.Location = new System.Drawing.Point(0, 858);
            this.panel_staff.Name = "panel_staff";
            this.panel_staff.Size = new System.Drawing.Size(247, 118);
            this.panel_staff.TabIndex = 5;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Linen;
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(0, 60);
            this.button2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.button2.Size = new System.Drawing.Size(247, 58);
            this.button2.TabIndex = 2;
            this.button2.Text = "Change Password";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Linen;
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(0, 0);
            this.button3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button3.Name = "button3";
            this.button3.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.button3.Size = new System.Drawing.Size(247, 60);
            this.button3.TabIndex = 1;
            this.button3.Text = "Profile";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Image = global::Tour.Properties.Resources.staff;
            this.button4.Location = new System.Drawing.Point(0, 797);
            this.button4.Name = "button4";
            this.button4.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.button4.Size = new System.Drawing.Size(247, 61);
            this.button4.TabIndex = 4;
            this.button4.Text = "Staff";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // panelManage
            // 
            this.panelManage.Controls.Add(this.button9);
            this.panelManage.Controls.Add(this.button8);
            this.panelManage.Controls.Add(this.btnEmployyeDatabase);
            this.panelManage.Controls.Add(this.btnVehicalDatabase);
            this.panelManage.Controls.Add(this.btnHotelDatabase);
            this.panelManage.Controls.Add(this.btnLocationDatabase);
            this.panelManage.Controls.Add(this.btndataCus);
            this.panelManage.Controls.Add(this.btnRoute);
            this.panelManage.Controls.Add(this.btnTicket);
            this.panelManage.Controls.Add(this.btnTour);
            this.panelManage.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelManage.Location = new System.Drawing.Point(0, 255);
            this.panelManage.Name = "panelManage";
            this.panelManage.Size = new System.Drawing.Size(247, 542);
            this.panelManage.TabIndex = 3;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Linen;
            this.button8.Dock = System.Windows.Forms.DockStyle.Top;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.Black;
            this.button8.Location = new System.Drawing.Point(0, 427);
            this.button8.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button8.Name = "button8";
            this.button8.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.button8.Size = new System.Drawing.Size(247, 55);
            this.button8.TabIndex = 8;
            this.button8.Text = "Misson Database";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // btnEmployyeDatabase
            // 
            this.btnEmployyeDatabase.BackColor = System.Drawing.Color.Linen;
            this.btnEmployyeDatabase.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEmployyeDatabase.FlatAppearance.BorderSize = 0;
            this.btnEmployyeDatabase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEmployyeDatabase.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmployyeDatabase.ForeColor = System.Drawing.Color.Black;
            this.btnEmployyeDatabase.Location = new System.Drawing.Point(0, 390);
            this.btnEmployyeDatabase.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEmployyeDatabase.Name = "btnEmployyeDatabase";
            this.btnEmployyeDatabase.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnEmployyeDatabase.Size = new System.Drawing.Size(247, 37);
            this.btnEmployyeDatabase.TabIndex = 7;
            this.btnEmployyeDatabase.Text = "Employee Database";
            this.btnEmployyeDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEmployyeDatabase.UseVisualStyleBackColor = false;
            this.btnEmployyeDatabase.Click += new System.EventHandler(this.btnEmployyeDatabase_Click);
            // 
            // btnVehicalDatabase
            // 
            this.btnVehicalDatabase.BackColor = System.Drawing.Color.Linen;
            this.btnVehicalDatabase.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnVehicalDatabase.FlatAppearance.BorderSize = 0;
            this.btnVehicalDatabase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVehicalDatabase.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVehicalDatabase.ForeColor = System.Drawing.Color.Black;
            this.btnVehicalDatabase.Location = new System.Drawing.Point(0, 360);
            this.btnVehicalDatabase.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnVehicalDatabase.Name = "btnVehicalDatabase";
            this.btnVehicalDatabase.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnVehicalDatabase.Size = new System.Drawing.Size(247, 30);
            this.btnVehicalDatabase.TabIndex = 6;
            this.btnVehicalDatabase.Text = "Vehical Database";
            this.btnVehicalDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVehicalDatabase.UseVisualStyleBackColor = false;
            this.btnVehicalDatabase.Click += new System.EventHandler(this.btnVehicalDatabase_Click);
            // 
            // btnHotelDatabase
            // 
            this.btnHotelDatabase.BackColor = System.Drawing.Color.Linen;
            this.btnHotelDatabase.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnHotelDatabase.FlatAppearance.BorderSize = 0;
            this.btnHotelDatabase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHotelDatabase.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHotelDatabase.ForeColor = System.Drawing.Color.Black;
            this.btnHotelDatabase.Location = new System.Drawing.Point(0, 300);
            this.btnHotelDatabase.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnHotelDatabase.Name = "btnHotelDatabase";
            this.btnHotelDatabase.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnHotelDatabase.Size = new System.Drawing.Size(247, 60);
            this.btnHotelDatabase.TabIndex = 5;
            this.btnHotelDatabase.Text = "Hotel Database";
            this.btnHotelDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHotelDatabase.UseVisualStyleBackColor = false;
            this.btnHotelDatabase.Click += new System.EventHandler(this.btnHotelDatabase_Click);
            // 
            // btnLocationDatabase
            // 
            this.btnLocationDatabase.BackColor = System.Drawing.Color.Linen;
            this.btnLocationDatabase.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLocationDatabase.FlatAppearance.BorderSize = 0;
            this.btnLocationDatabase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLocationDatabase.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLocationDatabase.ForeColor = System.Drawing.Color.Black;
            this.btnLocationDatabase.Location = new System.Drawing.Point(0, 240);
            this.btnLocationDatabase.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnLocationDatabase.Name = "btnLocationDatabase";
            this.btnLocationDatabase.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnLocationDatabase.Size = new System.Drawing.Size(247, 60);
            this.btnLocationDatabase.TabIndex = 4;
            this.btnLocationDatabase.Text = "Location Database";
            this.btnLocationDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLocationDatabase.UseVisualStyleBackColor = false;
            this.btnLocationDatabase.Click += new System.EventHandler(this.btnLocationDatabase_Click);
            // 
            // btndataCus
            // 
            this.btndataCus.BackColor = System.Drawing.Color.Linen;
            this.btndataCus.Dock = System.Windows.Forms.DockStyle.Top;
            this.btndataCus.FlatAppearance.BorderSize = 0;
            this.btndataCus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btndataCus.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndataCus.ForeColor = System.Drawing.Color.Black;
            this.btndataCus.Location = new System.Drawing.Point(0, 180);
            this.btndataCus.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btndataCus.Name = "btndataCus";
            this.btndataCus.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btndataCus.Size = new System.Drawing.Size(247, 60);
            this.btndataCus.TabIndex = 3;
            this.btndataCus.Text = "Ticket Database";
            this.btndataCus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btndataCus.UseVisualStyleBackColor = false;
            this.btndataCus.Click += new System.EventHandler(this.btndataCus_Click);
            // 
            // btnRoute
            // 
            this.btnRoute.BackColor = System.Drawing.Color.Linen;
            this.btnRoute.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRoute.FlatAppearance.BorderSize = 0;
            this.btnRoute.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRoute.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRoute.ForeColor = System.Drawing.Color.Black;
            this.btnRoute.Location = new System.Drawing.Point(0, 120);
            this.btnRoute.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRoute.Name = "btnRoute";
            this.btnRoute.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnRoute.Size = new System.Drawing.Size(247, 60);
            this.btnRoute.TabIndex = 0;
            this.btnRoute.Text = "Ticket Management";
            this.btnRoute.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRoute.UseVisualStyleBackColor = false;
            this.btnRoute.Click += new System.EventHandler(this.btnRoute_Click);
            // 
            // btnTicket
            // 
            this.btnTicket.BackColor = System.Drawing.Color.Linen;
            this.btnTicket.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnTicket.FlatAppearance.BorderSize = 0;
            this.btnTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTicket.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTicket.ForeColor = System.Drawing.Color.Black;
            this.btnTicket.Location = new System.Drawing.Point(0, 60);
            this.btnTicket.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnTicket.Name = "btnTicket";
            this.btnTicket.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnTicket.Size = new System.Drawing.Size(247, 60);
            this.btnTicket.TabIndex = 2;
            this.btnTicket.Text = "Tour Management";
            this.btnTicket.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTicket.UseVisualStyleBackColor = false;
            this.btnTicket.Click += new System.EventHandler(this.btnTicket_Click);
            // 
            // btnTour
            // 
            this.btnTour.BackColor = System.Drawing.Color.Linen;
            this.btnTour.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnTour.FlatAppearance.BorderSize = 0;
            this.btnTour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTour.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTour.ForeColor = System.Drawing.Color.Black;
            this.btnTour.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTour.Location = new System.Drawing.Point(0, 0);
            this.btnTour.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnTour.Name = "btnTour";
            this.btnTour.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnTour.Size = new System.Drawing.Size(247, 60);
            this.btnTour.TabIndex = 1;
            this.btnTour.Text = "Route Management";
            this.btnTour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTour.UseVisualStyleBackColor = false;
            this.btnTour.Click += new System.EventHandler(this.btnTour_Click);
            // 
            // btnManage
            // 
            this.btnManage.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnManage.FlatAppearance.BorderSize = 0;
            this.btnManage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManage.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManage.ForeColor = System.Drawing.Color.Black;
            this.btnManage.Image = global::Tour.Properties.Resources.manageicon1;
            this.btnManage.Location = new System.Drawing.Point(0, 206);
            this.btnManage.Name = "btnManage";
            this.btnManage.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnManage.Size = new System.Drawing.Size(247, 49);
            this.btnManage.TabIndex = 2;
            this.btnManage.Text = "Manage";
            this.btnManage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnManage.UseVisualStyleBackColor = true;
            this.btnManage.Click += new System.EventHandler(this.btnManage_Click);
            // 
            // panel_logo
            // 
            this.panel_logo.BackColor = System.Drawing.Color.LightCoral;
            this.panel_logo.Controls.Add(this.label1);
            this.panel_logo.Controls.Add(this.pictureBox2);
            this.panel_logo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_logo.Location = new System.Drawing.Point(0, 0);
            this.panel_logo.Name = "panel_logo";
            this.panel_logo.Size = new System.Drawing.Size(247, 206);
            this.panel_logo.TabIndex = 1;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Tour.Properties.Resources.ez1;
            this.pictureBox2.Location = new System.Drawing.Point(78, 29);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(87, 62);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.picBackground);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(264, -27);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(776, 667);
            this.panel4.TabIndex = 19;
            // 
            // picBackground
            // 
            this.picBackground.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picBackground.Image = global::Tour.Properties.Resources._0;
            this.picBackground.Location = new System.Drawing.Point(0, 122);
            this.picBackground.Name = "picBackground";
            this.picBackground.Size = new System.Drawing.Size(776, 545);
            this.picBackground.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBackground.TabIndex = 0;
            this.picBackground.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Aqua;
            this.panel1.BackgroundImage = global::Tour.Properties.Resources.Tour1;
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(264, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(776, 92);
            this.panel1.TabIndex = 20;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::Tour.Properties.Resources.manageicon;
            this.pictureBox3.Location = new System.Drawing.Point(249, 1);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(108, 88);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(1014, -7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 24);
            this.label4.TabIndex = 2;
            this.label4.Text = "-";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(1053, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 24);
            this.label3.TabIndex = 1;
            this.label3.Text = "X";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MediumBlue;
            this.label2.Location = new System.Drawing.Point(250, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(575, 91);
            this.label2.TabIndex = 0;
            this.label2.Text = "EZ - TRAVEL";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Linen;
            this.button9.Dock = System.Windows.Forms.DockStyle.Top;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.Black;
            this.button9.Location = new System.Drawing.Point(0, 482);
            this.button9.Margin = new System.Windows.Forms.Padding(2);
            this.button9.Name = "button9";
            this.button9.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.button9.Size = new System.Drawing.Size(247, 55);
            this.button9.TabIndex = 9;
            this.button9.Text = "Group Database";
            this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // SelectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1040, 640);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel_slide);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "SelectForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SelectForm";
            this.Load += new System.EventHandler(this.SelectForm_Load);
            this.panel_slide.ResumeLayout(false);
            this.panel_Help.ResumeLayout(false);
            this.panel_staff.ResumeLayout(false);
            this.panelManage.ResumeLayout(false);
            this.panel_logo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBackground)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel_slide;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panelManage;
        private System.Windows.Forms.Button btnManage;
        private System.Windows.Forms.Panel panel_logo;
        private System.Windows.Forms.Panel panel_staff;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel_Help;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button btnRoute;
        private System.Windows.Forms.Button btnTicket;
        private System.Windows.Forms.Button btnTour;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btndataCus;
        private System.Windows.Forms.PictureBox picBackground;
        private System.Windows.Forms.Button btnHotelDatabase;
        private System.Windows.Forms.Button btnLocationDatabase;
        private System.Windows.Forms.Button btnEmployyeDatabase;
        private System.Windows.Forms.Button btnVehicalDatabase;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
    }
}